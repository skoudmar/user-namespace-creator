.PHONY: all clean

all: userns

userns: main.c
	gcc -Wall -Os main.c -o userns

clean:
	rm userns