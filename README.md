# User namespace creator

Creates user namespace acording to first entry for current user in `/etc/subuid` and `/etc/subgid`, and then runs provided command.

## Usage

```sh
./userns <command>
```
### Example
Run `ls -ahl` in new user namespace.
```sh
./userns ls -ahl
```

## Compilation

run `make`

## Requirement

Commands `newuidmap` and `newgidmap`.
Install using:
```sh
apt install uidmap
```

## Credits

parts of code were taken from example at `man 7 user_namespaces`