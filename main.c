/**
 * Heavily inspired by example from https://man7.org/linux/man-pages/man7/user_namespaces.7.html
 */

#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pwd.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// stack size for child
#define STACK_SIZE (8 * 1024 * 1024)

struct child_args {
    char **argv;    /* Command to be executed by child, with args */
    int pipe_fd[2]; /* Pipe used to synchronize parent and child */
};

enum mapping {
    USER,
    GROUP
};

struct subid_entry {
    unsigned start_range;
    unsigned size;
};

static char child_stack[STACK_SIZE];

static int childFunc(void *arg) {
    struct child_args *args = arg;
    char ch;

    /* Wait until the parent has updated the UID and GID mappings.
       See the comment in main(). We wait for end of file on a
       pipe that will be closed by the parent process once it has
       updated the mappings. */

    close(args->pipe_fd[1]); /* Close our descriptor for the write
                                end of the pipe so that we see EOF
                                when parent closes its descriptor. */
    if (read(args->pipe_fd[0], &ch, 1) != 0) {
        fprintf(stderr, "Failure in child: read from pipe returned != 0\n");
        exit(EXIT_FAILURE);
    }

    close(args->pipe_fd[0]);

    /* Execute a shell command. */
    uid_t uid = getuid();
    gid_t gid = getgid();

    printf("Executing %s as %u:%u\n", args->argv[0], uid, gid);

    execvp(args->argv[0], args->argv);
    perror("execvp");
    exit(EXIT_FAILURE);
}

/**
 * @param type USER or GROUP
 * @param uid The USER id to provide mapping for. Provide uid even if querying for group ids!
 * @param entry destination for first mapping for user or group
 * @return 0 on success, -1 if no mapping found
 */
static int read_subid_entry(enum mapping type, uid_t uid, struct subid_entry *entry) {
    char *subid_file = (type == USER) ? "/etc/subuid" : "/etc/subgid";
    char id_str_num[20];
    snprintf(id_str_num, sizeof(id_str_num), "%u", uid);
    char *id_str;

    struct passwd *pwd = getpwuid(uid);
    if (pwd != NULL) {
        // get the name of the user
        id_str = pwd->pw_name;
    } else {
        id_str = id_str_num;
    }

    char *subid_line = NULL;
    size_t subid_line_size = 0;

    FILE *subid_file_fd = fopen(subid_file, "r");
    if (subid_file_fd == NULL) {
        fprintf(stderr, "Failed to open %s\n", subid_file);
        exit(1);
    }

    while (getline(&subid_line, &subid_line_size, subid_file_fd) != -1) {
        char *id = strtok(subid_line, ":");
        char *start_range = strtok(NULL, ":");
        char *size = strtok(NULL, ":");
        if (id == NULL || start_range == NULL || size == NULL) {
            fprintf(stderr, "Error parsing %s. Could not parse \"%s\"\n", subid_file, subid_line);
            exit(EXIT_FAILURE);
        }
        unsigned start_range_int = strtoul(start_range, NULL, 10);
        unsigned size_int = strtoul(size, NULL, 10);
        if (strcmp(id, id_str_num) == 0 || strcmp(id, id_str) == 0) {
            entry->start_range = start_range_int;
            entry->size = size_int;
            free(subid_line);
            return 0;
        }
    }
    free(subid_line);
    return -1;
}

static void write_mapping_all(enum mapping type, pid_t child) {
    char cmd[PATH_MAX];

    uid_t uid = getuid();
    uid_t id = (type == USER) ? getuid() : getgid();

    struct subid_entry entry;
    if (read_subid_entry(type, uid, &entry) == -1) {
        fprintf(stderr, "Failed to find mapping for uid: %u of type %s\n",
                uid, type == USER ? "USER" : "GROUP");
        exit(EXIT_FAILURE);
    }

    snprintf(cmd, sizeof(cmd), "new%cidmap %u 0 %u 1 1 %u %u",
             (type == USER) ? 'u' : 'g',
             child,
             id,
             entry.start_range,
             entry.size);

    int ret = system(cmd);
    if (ret != 0) {
        fprintf(stderr, "Failed to execute %s\n", cmd);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char **argv) {
    pid_t child_pid;
    struct child_args args;
    int flags = CLONE_NEWUSER;

    if (argc < 2 || strcmp(argv[1], "-h") == 0) {
        printf("Maps root:root to uid:gid and 1-... is mapped based on /etc/subuid and /etc/subgid\n");
        printf("\nUsage: %1$s command\nExample: %1$s ls -ahl\n", argv[0]);
        exit(0);
    }

    args.argv = &argv[1];

    // pipe used for synchronization
    if (pipe(args.pipe_fd)) {
        perror("pipe()");
        exit(1);
    }

    child_pid = clone(childFunc, child_stack + STACK_SIZE, flags | SIGCHLD, &args);
    if (child_pid == -1) {
        perror("clone()");
        exit(1);
    } else {
        close(args.pipe_fd[0]);
        fclose(stdin);
        fclose(stdout);
        // keep stderr open for error messages
    }

    write_mapping_all(USER, child_pid);
    write_mapping_all(GROUP, child_pid);

    /* Close the write end of the pipe, to signal to the child that we
       have updated the UID and GID maps. */
    close(args.pipe_fd[1]);

    int status = 0;
    if (waitpid(child_pid, &status, 0) == -1) { /* Wait for child */
        perror("waitpid");
        exit(1);
    }

    return status;
}
